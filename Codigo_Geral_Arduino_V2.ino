#include <Ultrasonic.h>
float S1,S2,S3,S4,S5,S6,S7,S8;
int infra_frente = 22;
int infra_direita = 23;
int infra_esquerda = 24;
int infra_traseira = 25;
Ultrasonic ultra_frente(26,2); // (pino_trigger,pino_echo)
Ultrasonic ultra_direita(27,3); // (pino_trigger,pino_echo)
Ultrasonic ultra_esquerda(28,4); // (pino_trigger,pino_echo)
Ultrasonic ultra_traseira(29,5); // (pino_trigger,pino_echo)
void setup() {
  Serial.begin(9600);
  pinMode(infra_frente, INPUT);
  pinMode(infra_direita, INPUT);
  pinMode(infra_esquerda, INPUT);
  pinMode(infra_traseira, INPUT);
}
void loop() {
  float S1;
  long microsec_us_1 = ultra_frente.timing();
  S1 = ultra_frente.convert(microsec_us_1, Ultrasonic::CM);

 

  float S2;
  long microsec_us_2 = ultra_direita.timing();
  S2 = ultra_direita.convert(microsec_us_2, Ultrasonic::CM);

 

  float S3;
  long microsec_us_3 = ultra_esquerda.timing();
  S3 = ultra_esquerda.convert(microsec_us_3, Ultrasonic::CM);
  
  float S4;
  long microsec_us_4 = ultra_traseira.timing();
  S4 = ultra_traseira.convert(microsec_us_4, Ultrasonic::CM);
  
  
  if(digitalRead(infra_frente) == LOW){
    S5=1;
  }else{
    S5=0;
  }
   
  if(digitalRead(infra_direita) == LOW){
    S6=1;
  }else{
    S6=0;
  }
  
  if(digitalRead(infra_esquerda) == LOW){
    S7=1;
  }else{
    S7=0;
  }
  if(digitalRead(infra_traseira) == LOW){
    S8=1;
  }else{
    S8=0;
  }
  Serial.print(S1);
  Serial.print(", ");
  Serial.print(S2);
  Serial.print(", ");
  Serial.print(S3);
  Serial.print(", ");
  Serial.print(S4);
  Serial.print(", ");
  Serial.print(S5);
  Serial.print(", ");
  Serial.print(S6);
  Serial.print(", ");
  Serial.print(S7);
  Serial.print(", ");
  Serial.println(S8);
  delay(200);
}
