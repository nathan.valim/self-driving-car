from imageai.Detection import ObjectDetection
import os

execution_path = os.getcwd()
print(execution_path)

path_models = "models\\"
path_images = "images\\"

detector = ObjectDetection()
detector.setModelTypeAsRetinaNet()
detector.setModelPath(os.path.join(execution_path, path_models + "resnet50_coco_best_v2.0.1.h5"))
detector.loadModel()
detections = detector.detectObjectsFromImage(
    input_image=os.path.join(execution_path, path_images + "DeuCerto2.jpg"),
    output_image_path=os.path.join(execution_path, path_images + "_almoxarifado1_test_new.jpg")
)

for eachObject in detections:
    print(eachObject["name"], " : ", eachObject["percentage_probability"])

# link: https://towardsdatascience.com/object-detection-with-10-lines-of-code-d6cb4d86f606
