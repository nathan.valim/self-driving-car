# shapes and texts
import cv2
import numpy as np

img = np.zeros((512, 512, 3))  # imagem com matriz de 0's, pois 0 significa preto -> grayscale
# print(img.shape)

# img[:] = 255, 0, 0  # colorindo toda a imagem (matriz) de azul -> BGR
# img[200:300, 100:300] = 0, 100, 50  # colorindo parte da matriz range de altura, range de largura

# posição em que começa, que termina, cor, tamanho da borda
# img.shape[0] -> height
# img.shape[1] -> width
cv2.line(img, (0, 0), (img.shape[1], img.shape[0]), (0, 200, 0), 3)
cv2.rectangle(img, (0, 0), (250, 350), (0, 0, 240), cv2.FILLED)
cv2.circle(img, (400, 50), 30, (40, 255, 0), 5)
cv2.putText(img, "OPEN CV", (310, 150), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 150, 0), 1)

cv2.imshow("Image", img)

cv2.waitKey(0)
